package com.itau.Loja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.Loja.model.OfertaModel;
import com.itau.Loja.repository.OfertaRepository;

@Controller
public class OfertaController {
	@Autowired
	OfertaRepository ofertaRepository;
	

	@RequestMapping(path="/insereoferta", method=RequestMethod.POST)
	@ResponseBody
	public OfertaModel inserirOferta(@RequestBody OfertaModel oferta) {
		return ofertaRepository.save(oferta);
	}
	
	@RequestMapping(path="/consultaoferta", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<OfertaModel> consultarOferta() {		
		return ofertaRepository.findAll();
	}
	
}
